package main

import (
	"testing"
)

var Sample = &Diff{
	Type: DiffModify,
	Desc: "first level {\n%s\n}",
	Subdiffs: []*Diff{
		&Diff{
			Type: DiffModify,
			Desc: "second level",
			Subdiffs: []*Diff{
				&Diff{
					Type: DiffDelete,
					Desc: "third level",
				},
				&Diff{
					Type: DiffAdd,
					Desc: "also third level",
				},
			},
		},
	},
}

func TestDiffString(t *testing.T) {
	var expected = `
M	first level {
	M	second level
		D	third level
		A	also third level
	}
`[1:]

	if ds := Sample.DiffString(0); ds != expected {
		t.Errorf("Expected\n%s\n, got\n%s\n", expected, ds)
	}
}

func TestShouldShow(t *testing.T) {
	oFlagShow := FlagShow
	test := func(diff *Diff, expected bool) {
		if actual := diff.ShouldShow(); actual != expected {
			t.Errorf("expected ShouldShow() %t, got %t for %+v", actual, expected, diff)
		}
	}
	FlagShow = "M"
	test(Sample, false)
	FlagShow = "A"
	test(Sample, false)
	FlagShow = "D"
	test(Sample, false)

	FlagShow = "DM"
	test(Sample.Subdiffs[0], true)
	test(Sample.Subdiffs[0].Subdiffs[0], true)
	test(Sample.Subdiffs[0].Subdiffs[1], false)
	FlagShow = "Dm"
	test(Sample, true)
	test(Sample.Subdiffs[0].Subdiffs[0], true)
	test(Sample.Subdiffs[0].Subdiffs[1], false)
	Sample.Subdiffs[0].Subdiffs[0].Type = DiffModify
	test(Sample.Subdiffs[0].Subdiffs[0], false)
	test(Sample.Subdiffs[0], false)
	test(Sample, false)
	FlagShow = oFlagShow
}
