package main

import (
	"testing"
)

func TestPackageDiff(t *testing.T) {
	const OldPackage = `
	func RemovedFunc() {}
	func ChangedFunc(a int) {}
	func ChangedToType() {}
	func DeclaresTypes() { type MyVal int; }`
	const NewPackage = `
	func NewFunc() {}
	type ChangedToType int
	func ChangedFunc(b real) string { return "f" }
	func DeclaresTypes() { type MyVal string; }`
	paOld := populateFrom(t, prefixPkg(OldPackage))
	paNew := populateFrom(t, prefixPkg(NewPackage))
	var diffStrings []string
	diffs := paOld.Diff(paNew)
	for _, d := range diffs {
		diffStrings = append(diffStrings, d.DiffString(0))
	}
	expected := []string{"D\tfunc RemovedFunc()",
		"M\t-func ChangedFunc(int)\n\t+func ChangedFunc(real) string",
		"M\t-func ChangedToType()\n\t+type ChangedToType int", "A\tfunc NewFunc()"}
	if len(diffStrings) != len(expected) {
		t.Fatalf("Expected diffs: %+v, got: %+v", expected, diffStrings)
	}
	for i, _ := range diffStrings {
		if diffStrings[i] != expected[i]+"\n" {
			t.Errorf("Expected diff\n%s\n, got\n%s", expected[i]+"\n", diffStrings[i])
		}
	}
}
