package main

import (
	"fmt"
	"go/ast"
)

// a FieldSet contains functionality that can determine the difference between two fieldlists
type FieldSet struct {
	named  map[string]string // field name -> field type
	anons  map[string]int    // field type -> count
	fields []*ast.Field
}

// NewFieldSet returns a fieldset from a FieldList
func NewFieldSet(fl *ast.FieldList) DiffableSet {
	newSet := &FieldSet{
		named:  make(map[string]string),
		anons:  make(map[string]int),
		fields: fl.List,
	}
	var anonCount int
	for _, f := range fl.List {
		if f.Names == nil {
			anonCount++
			newSet.anons[nodeToString(f.Type)]++
			continue
		}
		for _, ident := range f.Names {
			newSet.named[ident.Name] = nodeToString(f.Type)
		}
	}
	total := anonCount + len(newSet.named)
	if total != fl.NumFields() {
		panic(fmt.Errorf("Expected %d fields, got %d in %+v", fl.NumFields(), total, fl))
	}
	return newSet
}

// Difference returns diffs for fields that are in receiver but not in param
// FieldSet.  It is the caller's responsibility to populate the Type field of returned Diffs.
func (fs FieldSet) Difference(other DiffableSet) []*Diff {
	var diffs []*Diff
	otherSet := other.(*FieldSet)
	for ident, ftype := range fs.named {
		if _, ok := otherSet.named[ident]; !ok {
			diffs = append(diffs, &Diff{Desc: ident + " " + ftype})
		}
	}
	for ftype, count := range fs.anons {
		c := count - otherSet.anons[ftype] // can be negative
		for i := 0; i < c; i++ {
			diffs = append(diffs, &Diff{Desc: ftype})
		}
	}
	return diffs
}

// Modified returns the named fields in receiver that have modified types in
// param FieldSet.  Modified is not defined for anonymous fields as intent cannot be determined.
func (fs FieldSet) Modified(other DiffableSet) []*Diff {
	var diffs []*Diff
	otherSet := other.(*FieldSet)
	for ident, ftype := range fs.named {
		if otherType, ok := otherSet.named[ident]; ok && ftype != otherType {
			thisTypeNode := &Node{N: fs.findFieldType(ident)}
			otherTypeNode := &Node{N: otherSet.findFieldType(ident)}
			diffs = append(diffs, thisTypeNode.TypeDiff(ident, otherTypeNode))
		}
	}
	return diffs
}

// findFieldType returns the type node of a named field given its identifier name.
func (fs *FieldSet) findFieldType(name string) ast.Node {
	for _, f := range fs.fields {
		if f.Names == nil {
			continue
		}
		for _, ident := range f.Names {
			if ident.Name == name {
				return f.Type.(ast.Node)
			}
		}
	}
	return nil
}
