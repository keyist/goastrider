package main

import (
	"fmt"
	"go/ast"
	"go/token"
)

// a InterfaceSet contains functionality that can determine the difference between two fieldlists
type InterfaceSet struct {
	named  map[string]string // field name -> field type
	embeds map[string]bool   // interface type name -> bool presence
	fields []*ast.Field
}

// NewInterfaceSet returns a Diffable given a FieldList
func NewInterfaceSet(fl *ast.FieldList) DiffableSet {
	newSet := InterfaceSet{
		named:  make(map[string]string),
		embeds: make(map[string]bool),
		fields: fl.List,
	}
	for _, f := range fl.List {
		switch t := f.Type.(type) {
		case *ast.FuncType:
			newSet.named[f.Names[0].Name] = nodeToString(f.Type)
		case *ast.Ident:
			newSet.embeds[t.Name] = true
		case *ast.SelectorExpr: // somepkg.SomeInterface
			newSet.embeds[nodeToString(t)] = true
		default:
			if FlagDebug {
				ast.Print(&token.FileSet{}, f)
			}
			panic(fmt.Errorf("Unexpected interface node %T", t))
		}
	}
	total := len(newSet.named) + len(newSet.embeds)
	if total != fl.NumFields() {
		panic(fmt.Errorf("Expected %d fields, got %d in %+v", fl.NumFields(), total, fl))
	}
	return newSet
}

// Difference returns diffs for fields that are in receiver but not in param
// InterfaceSet.  It is the caller's responsibility to populate the Type field of returned Diffs.
func (fs InterfaceSet) Difference(other DiffableSet) []*Diff {
	var diffs []*Diff
	otherSet := other.(InterfaceSet)
	for ident, ftype := range fs.named {
		if _, ok := otherSet.named[ident]; !ok {
			diffs = append(diffs, &Diff{Desc: ident + " " + ftype})
		}
	}
	for name, _ := range fs.embeds {
		if _, ok := otherSet.embeds[name]; !ok {
			diffs = append(diffs, &Diff{Desc: name})
		}
	}
	return diffs
}

// Modified returns the named fields in receiver that have modified types in
// param InterfaceSet.  Modified is not defined for anonymous fields as intent cannot be determined.
func (fs InterfaceSet) Modified(other DiffableSet) []*Diff {
	var diffs []*Diff
	otherSet := other.(InterfaceSet)
	for ident, ftype := range fs.named {
		if otherType, ok := otherSet.named[ident]; ok && ftype != otherType {
			thisTypeNode := &Node{N: fs.findFieldType(ident)}
			otherTypeNode := &Node{N: otherSet.findFieldType(ident)}
			diffs = append(diffs, thisTypeNode.FuncDiff(ident, otherTypeNode))
		}
	}
	return diffs
}

// findFieldType returns the type node of a named field given its identifier name.
func (fs *InterfaceSet) findFieldType(name string) ast.Node {
	for _, f := range fs.fields {
		if f.Names == nil {
			continue
		}
		for _, ident := range f.Names {
			if ident.Name == name {
				return f.Type.(ast.Node)
			}
		}
	}
	return nil
}
