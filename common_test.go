// common helpers for tests

package main

import (
	"go/parser"
	"go/token"
	"testing"
)

var fset = token.NewFileSet()

func prefixPkg(s string) string {
	return "package parser_test\n" + s
}

func populateFrom(t *testing.T, src string) PackageAPI {
	f, err := parser.ParseFile(fset, "", src, 0)
	if err != nil {
		t.Fatal(err)
		return nil
	}
	pa := make(PackageAPI)
	pa.populate(f)
	return pa
}
