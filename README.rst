Go/ast Rider
============

OVERVIEW
--------

goastrider is a command-line tool that performs a partial semantic diff given
two directories with Go source code.  It does this by parsing the given
directories and analyzing the resulting abstract syntax trees.

Changes are detected at the package level.  goastrider currently highlights the
following changes within common package name across the two directories:

- new/removed functions/methods
- functions/methods with changed type signature
- structs with new/removed/changed fields
- interfaces with new/removed/changed methods and embeds

Information on usage can be found in the man page or running ``goastrider -h``.

INSTALL
-------

sourcecode::

	go get bitbucket.org/keyist/goastrider
	go install bitbucket.org/keyist/goastrider

If $GOPATH/bin is in your path, you can now run it with: ``goastrider <old-path> <new-path>``

A convenience script ``grider.sh`` has been provided for easy diffing within a repo.  Presently this script only supports git repositories.

USE CASES
---------

- Go developers: get a concise summary of what's changed in dependencies before upgrading.  Or follow Go tip to see what's changed.

- Library authors: goastrider can help you check if you're breaking your public API, or help you prepare your release notes


KNOWN ISSUES
------------

1. The command needs two directories in order to run -- this can be inconvenient for many use cases.  The ``grider.sh`` script makes this easier.

2. Currently files using the cross-compile \*_GOOS.go, \*_GOARCH.go, or \*_GOOS_GOARCH.go conventions are skipped due to undefined results.
