package main

import (
	"fmt"
	"os"
	"strings"
)

// a Diff represents a change in use of public identifiers between two
// codebases. Diffs are recursive (a struct diff may have multiple type diffs
// on fields, an interface diff may have multiple func diffs on methods).
type Diff struct {
	Type     DiffType
	Desc     string
	Subdiffs []*Diff
}

type DiffType byte

// Diff DiffTypes enum
const (
	DiffAdd    = 'A'
	DiffDelete = 'D'
	DiffModify = 'M'
)

// DiffString returns a diff indented to the given level
func (d *Diff) DiffString(indent int) string {
	subdiffString := func() string {
		if len(d.Subdiffs) == 0 {
			return ""
		}
		var subdiffStrings []string
		for _, sd := range d.SubdiffsToShow() {
			subdiffStrings = append(subdiffStrings, sd.DiffString(indent+1))
		}
		return strings.Trim(strings.Join(subdiffStrings, ""), "\n")
	}()

	var dstr string
	// if a diff with subdiffs has Desc with a single %s, subdiffLines are
	// interpolated in, otherwise they are appended
	lines := strings.Split(d.formattedType()+"\t"+d.Desc, "\n")
	for i, line := range lines {
		var numTabs int
		switch {
		case strings.Count(line, "%s") == 1:
			// expanded subdiffs carry their own indent
			numTabs = 0
		case i == 0:
			numTabs = indent
		default:
			numTabs = indent + 1
		}
		dstr += strings.Repeat("\t", numTabs) + line + "\n"
	}
	if strings.Count(d.Desc, "%s") == 1 {
		dstr = fmt.Sprintf(dstr, subdiffString)
	} else {
		dstr = dstr + subdiffString
	}

	return dstr
}

// returns True if the --show flag contains the diff type.  If the flag
// contains 'm' and not M, modifications with visible subdiffs (add/delete)
// will be shown
func (d *Diff) ShouldShow() bool {
	var (
		visible     = len(d.Subdiffs) == 0 || len(d.SubdiffsToShow()) > 0
		typeEnabled = strings.Contains(FlagShow, string(d.Type))
		isSmallM    = strings.Contains(FlagShow, "m") && d.Type == DiffModify
	)
	switch {
	case typeEnabled:
		return visible
	case !typeEnabled && isSmallM:
		return len(d.SubdiffsToShow()) > 0
	default:
		return false
	}
	panic("All cases in switch must return")
}

func (d *Diff) SubdiffsToShow() []*Diff {
	var diffs []*Diff
	for _, sd := range d.Subdiffs {
		if sd.ShouldShow() {
			diffs = append(diffs, sd)
		}
	}
	return diffs
}

// colors
const (
	ColorRed   uint = 31
	ColorGreen uint = 32
)

func (d *Diff) Colorize(s string) string {
	fn := func() FormatFunc {
		switch d.Type {
		case DiffAdd:
			return greenText
		case DiffDelete:
			return redText
		case DiffModify:
			return defaultText
		default:
			panic("Switch not exhaustive")
		}
	}()
	return fn(s)
}

func (d *Diff) formattedType() string {
	return d.Colorize(string(d.Type))
}

type FormatFunc func(string) string

func redText(s string) string {
	return colorText(s, ColorRed)
}

func greenText(s string) string {
	return colorText(s, ColorGreen)
}

func defaultText(s string) string {
	return s
}

var tty bool

func init() {
	tty = isTTY(os.Stdout)
}

func colorText(s string, color uint) string {
	if !tty {
		return s
	}
	return fmt.Sprintf("\033[0;%dm%s\033[0m", color, s)
}
