// Dumping ground for anything that imports C so as not to affect coverage

package main

import (
	"os"
)

/*
#include <unistd.h>
*/
import "C"

func isTTY(file *os.File) bool {
	return int(C.isatty(C.int(file.Fd()))) != 0
}
