.DEFAULT_GOAL := test

all:
	@mkdir -p bin/
	@go build -o bin/goastrider

clean:
	@go clean ./...

test:
	@go test ./...

.PHONY: all clean test
