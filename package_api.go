package main

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"os"
	"path/filepath"
	"strings"
)

// a PackageAPI maps identifiers to Nodes
type PackageAPI map[string]*Node

// PackageDiff parses ASTs for oldPath and newPath and outputs their diffs
func PackageDiff(oldPath, newPath string, pkgPrefix string) ([]*Diff, error) {
	var diffs []*Diff
	oldPkgs, err := parseDir(oldPath)
	if err != nil {
		return nil, err
	}
	newPkgs, err := parseDir(newPath)
	if err != nil {
		return nil, err
	}
	addPath := func(name string) string {
		if pkgPrefix == "" {
			return name
		}
		if d, immediate := filepath.Split(pkgPrefix); immediate == name {
			pkgPrefix = d
		}
		return strings.Trim(pkgPrefix, "/") + "/" + name
	}
	for name, _ := range oldPkgs {
		if _, ok := newPkgs[name]; !ok {
			diffs = append(diffs, &Diff{
				Type: DiffDelete,
				Desc: addPath(name),
			})
			continue
		}
		if !FlagWithUnexported {
			ast.PackageExports(oldPkgs[name])
			ast.PackageExports(newPkgs[name])
		}
		oldPa := make(PackageAPI)
		newPa := make(PackageAPI)
		oldPa.populate(oldPkgs[name])
		newPa.populate(newPkgs[name])
		pkgDiffs := oldPa.Diff(newPa)
		if len(pkgDiffs) == 0 {
			Warn("Package %s: no changes detected\n", name)
			continue
		}
		diffs = append(diffs, &Diff{
			Type: DiffModify, Desc: addPath(name),
			Subdiffs: pkgDiffs,
		})
	}
	for name, _ := range newPkgs {
		if _, ok := oldPkgs[name]; !ok {
			diffs = append(diffs, &Diff{
				Type: DiffAdd, Desc: addPath(name),
			})
		}
	}
	return diffs, nil
}

// Visit lets PackageAPI implement the ast.Visitor interface
func (pa PackageAPI) Visit(node ast.Node) (w ast.Visitor) {
	if k, v := (&Node{N: node}).KeyValue(); v != nil {
		pa[k] = v
	}
	if _, ok := node.(*ast.FuncDecl); ok {
		// do not Visit nodes inside a FuncDecl to avoid picking up type identifiers in function scope
		// case #6 in http://golang.org/ref/spec#Declarations_and_scope
		return nil
	}
	return pa
}

// Diff returns a list of diffs between pa and other
func (pa PackageAPI) Diff(other PackageAPI) (diffs []*Diff) {
	for entity, node := range pa {
		diff := func() *Diff {
			switch otherNode, ok := other[entity]; {
			case ok && node.SameType(otherNode):
				return node.Diff(otherNode)
			case !ok:
				return &Diff{
					Type: DiffDelete,
					Desc: redText(node.String()),
				}
			case !node.SameType(otherNode):
				return &Diff{
					Type: DiffModify,
					Desc: fmt.Sprintf("%s\n%s",
						redText("-"+node.String()), greenText("+"+otherNode.String())),
				}
			default:
				panic("Switch not exhaustive")
			}
		}()
		if diff != nil {
			diffs = append(diffs, diff)
		}
	}

	for otherEntity, otherNode := range other {
		diff := func() *Diff {
			if _, ok := pa[otherEntity]; !ok {
				return &Diff{
					Type: DiffAdd,
					Desc: greenText(otherNode.String()),
				}
			}
			return nil
		}()
		if diff != nil {
			diffs = append(diffs, diff)
		}
	}
	return diffs
}

// populate walks through an ast.Node and adds recognized nodes to a PackageAPI map
func (pa PackageAPI) populate(node ast.Node) {
	ast.Walk(pa, node)
}

// parseDir parses .go files in the given path and returns a map of package
// names to package ASTs.  It does not recurse into subdirectories.
func parseDir(path string) (map[string]*ast.Package, error) {
	if FlagDebug {
		Warn("Parsing path: %s\n", path)
	}
	fset := token.NewFileSet()

	// build-constrained files currently have undefined results as they violate
	// our invariant that identifiers are never redefined within a package.
	// Better to return no results than wrong results, so for now we simply
	// skip them.  In the future they can probably be handled with special
	// namespacing.
	filterFn := func(fi os.FileInfo) bool {
		const buildSkipWarning = "Skipping build-constrained file %s\n"
		var (
			name     = fi.Name()
			testCond = FlagWithTests || !strings.HasSuffix(name, "_test.go")
		)
		// TODO detect build-directive comments
		if isBuildConstrained(name) {
			if testCond {
				Warn(buildSkipWarning, name)
			}
			return false
		}
		return testCond
	}
	return parser.ParseDir(fset, path, filterFn, 0)
}

// isBuildConstrained returns true if the given filename matches Go's rules for denoting files with build constraints
func isBuildConstrained(filename string) bool {
	// from build/syslist.go
	const (
		goosList   = "darwin dragonfly freebsd linux netbsd openbsd plan9 windows "
		goarchList = "386 amd64 arm "
	)
	// adapted from build.goodOSArchFile
	var (
		knownOS   = make(map[string]bool)
		knownArch = make(map[string]bool)
	)
	for _, v := range strings.Fields(goosList) {
		knownOS[v] = true
	}
	for _, v := range strings.Fields(goarchList) {
		knownArch[v] = true
	}
	if i := strings.LastIndex(filename, "."); i != -1 {
		filename = filename[:i]
	}
	parts := strings.Split(filename, "_")
	if l := len(parts); l > 0 && parts[l-1] == "test" {
		parts = parts[:l-1]
	}
	l := len(parts)
	if l >= 2 && knownOS[parts[l-2]] && knownArch[parts[l-1]] {
		return true
	}
	if l >= 1 && (knownOS[parts[l-1]] || knownArch[parts[l-1]]) {
		return true
	}
	return false
}
