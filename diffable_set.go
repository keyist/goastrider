package main

type DiffableSet interface {
	Difference(DiffableSet) []*Diff
	Modified(DiffableSet) []*Diff
}

func CompareSets(oldSet, newSet DiffableSet) []*Diff {
	var (
		removals  = oldSet.Difference(newSet)
		mods      = oldSet.Modified(newSet)
		additions = newSet.Difference(oldSet)
	)
	for _, d := range removals {
		d.Type = DiffDelete
		d.Desc = redText(d.Desc)
	}
	for _, d := range additions {
		d.Type = DiffAdd
		d.Desc = greenText(d.Desc)
	}
	var subdiffs []*Diff
	subdiffs = append(subdiffs, removals...)
	for _, mod := range mods {
		if mod != nil {
			subdiffs = append(subdiffs, mod)
		}
	}
	return append(subdiffs, additions...)
}
