package main

import (
	"fmt"
	"reflect"
	"testing"
)

const (
	StructDecl = `
type PublicStruct struct {
	PublicIdent  string
}`
	FuncDecl = `
func PublicFunc(a int) string {
	return "func"
}`
	MethodDecl = `
type Recv interface{}
func (r Recv) PublicMethod() {
}`
	StarMethodDecl = `
type Recv interface{}
func (r *Recv) PublicMethod() {
}`
)

func TestKeyValue(t *testing.T) {
	cases := []struct {
		expr              string
		expectedKey       string
		expectedValueType string
	}{
		{StructDecl, "PublicStruct", "*ast.TypeSpec"},
		{FuncDecl, "PublicFunc", "*ast.FuncDecl"},
		{MethodDecl, "Recv.PublicMethod", "*ast.FuncDecl"},
		{StarMethodDecl, "Recv.PublicMethod", "*ast.FuncDecl"},
	}
	for _, c := range cases {
		pa := populateFrom(t, prefixPkg(c.expr))
		if v, ok := pa[c.expectedKey]; ok {
			if vtype := reflect.TypeOf(v.N).String(); vtype != c.expectedValueType {
				t.Errorf("Expected value type '%s', got '%s'", c.expectedValueType, vtype)
			}
		} else {
			t.Errorf("Expected key %s to be present in %v", c.expectedKey, pa)
		}
	}
}

func TestFuncDiffArity(t *testing.T) {
	const (
		MoreArgsSameTypeFuncDecl    = `func PublicFunc(a,b int) string { return "f" }`
		MoreArgsMultiTypeFuncDecl   = `func PublicFunc(a int, b bool, c []byte) string { return "f" }`
		MoreArgsPointerTypeFuncDecl = `func PublicFunc(a int, f *float64) string { return "f" }`
		MoreArgsMapTypeFuncDecl     = `func PublicFunc(a int, m map[string]rune) string { return "f" }`
		LessArgs                    = `func PublicFunc() string { return "f" }`
		MoreReturns                 = `func PublicFunc(a int) (string, error) { return "f", nil }`
		MoreNamedReturns            = `func PublicFunc(a int) (s string, err error) { return "f", nil }`
		LessReturns                 = `func PublicFunc(a int) {}`
	)
	cases := []struct {
		expr         string
		newSignature string
	}{
		{MoreArgsSameTypeFuncDecl, "(int, int) string"},
		{MoreArgsMultiTypeFuncDecl, "(int, bool, []byte) string"},
		{MoreArgsPointerTypeFuncDecl, "(int, *float64) string"},
		{MoreArgsMapTypeFuncDecl, "(int, map[string]rune) string"},
		{LessArgs, "() string"},
		{MoreReturns, "(int) (string, error)"},
		{MoreNamedReturns, "(int) (string, error)"},
		{LessReturns, "(int)"},
	}
	paOld := populateFrom(t, prefixPkg(FuncDecl))
	for _, c := range cases {
		paNew := populateFrom(t, prefixPkg(c.expr))
		diff := paOld["PublicFunc"].Diff(paNew["PublicFunc"])
		expected := "-func PublicFunc(int) string\n+func PublicFunc" + c.newSignature
		if diff.Desc != expected {
			t.Errorf("Expected desc '%s', got '%s'", expected, diff.Desc)
		}
	}
}

func TestFuncDiffSignature(t *testing.T) {
	const (
		DifferentArgType    = `func PublicFunc(a ...int) string { return "f" }`
		DifferentResultType = `func PublicFunc(a int) interface{} { return nil }`
		SameArgName         = `func PublicFunc(a int) string { return "f" }`
		DifferentArgName    = `func PublicFunc(b int) string { return "f" }`
		DifferentResultName = `func PublicFunc(a int) (s string) { return nil }`
	)
	cases := []struct {
		expr         string
		newSignature string
	}{
		{DifferentArgType, "(...int) string"},
		{DifferentResultType, "(int) interface{}"},
		{SameArgName, ""},
		{DifferentArgName, ""},
		{DifferentResultName, ""},
	}
	paOld := populateFrom(t, prefixPkg(FuncDecl))
	for _, c := range cases {
		paNew := populateFrom(t, prefixPkg(c.expr))
		diff := paOld["PublicFunc"].Diff(paNew["PublicFunc"])
		if c.newSignature == "" {
			if diff != nil {
				t.Errorf("Expected nil diff, got %+v", diff)
			}
			continue
		}
		expected := "-func PublicFunc(int) string\n+func PublicFunc" + c.newSignature
		if diff.Desc != expected {
			t.Errorf("Expected desc '%s', got '%s'", expected, diff.Desc)
		}
	}
}

func TestTypeSpecChange(t *testing.T) {
	const TypeDecl = "type MyType %s"
	var types = []string{
		"bool", "byte",
		"int8", "int16", "int32", "int64",
		"uint8", "uint16", "uint32", "uint64",
		"float32", "float64",
		"string",
		"int", "uint", "uintptr", "float", "struct{ x int }",
		"chan bool", "<-chan byte", "chan<- *string", "chan (<-chan int)",
	}
	for _, from := range types {
		paOld := populateFrom(t, prefixPkg(fmt.Sprintf(TypeDecl, from)))
		for _, to := range types {
			paNew := populateFrom(t, prefixPkg(fmt.Sprintf(TypeDecl, to)))
			diff := paOld["MyType"].Diff(paNew["MyType"])
			if from == to {
				if diff != nil {
					t.Errorf("Expected diff between same type %s to be nil, got %+v", to, diff)
				}
				continue
			}
			if diff == nil {
				t.Errorf("Expected diff between %s and %s to not be nil", from, to)
				continue
			}
			if diff.Desc != fmt.Sprintf("-type MyType %s\n+type MyType %s", from, to) {
				t.Errorf("Diff between %s and %s had wrong values: %#v", from, to, diff)
			}
		}
	}
}

func TestStructChange(t *testing.T) {
	const StructPrefix = "type MyStruct struct "
	cases := []struct {
		oldDef   string
		newDef   string
		expected string
	}{
		// named field changes, additions
		{
			"{ X int; Y string }",
			"{ X int; Y []byte; Z bool }",
			`
M	type MyStruct struct {
	M	-type Y string
		+type Y []byte
	A	Z bool
	}`[1:]},
		// embed changes
		{
			"{ o OtherStruct; OtherStruct }",
			"{ b OtherStruct; *OtherStruct }",
			`
M	type MyStruct struct {
	D	o OtherStruct
	D	OtherStruct
	A	b OtherStruct
	A	*OtherStruct
	}`[1:]},

		// anon field changes
		{
			"{ int; *string; i interface{} }",
			"{ string; int; bool }",
			`
M	type MyStruct struct {
	D	i interface{}
	D	*string
	A	string
	A	bool
	}`[1:]},

		// named and anon changes combined; function and channel types
		{
			"{ F func(int) string ; b chan<- bool ; c chan<- *uint; float; float }",
			"{ F func(bool) *string ; b <-chan bool; c chan<- uint }",
			`
M	type MyStruct struct {
	D	float
	D	float
	M	-type F func(int) string
		+type F func(bool) *string
	M	-type b chan<- bool
		+type b <-chan bool
	M	-type c chan<- *uint
		+type c chan<- uint
	}`[1:]},

		// unchanged, no diff
		{
			"{ foo struct { int; int } }",
			"{ foo struct { int; int } }",
			""},

		// nested structs
		{
			"{ foo struct { int; int } }",
			"{ foo struct { bool; int }; bar [123]struct{int; int} }",
			`
M	type MyStruct struct {
	M	type foo struct {
		D	int
		A	bool
		}
	A	bar [123]struct {
			int
			int
		}
	}`[1:]},
	}
	for _, c := range cases {
		var (
			paOld = populateFrom(t, prefixPkg(StructPrefix+c.oldDef))
			paNew = populateFrom(t, prefixPkg(StructPrefix+c.newDef))
			diff  = paOld["MyStruct"].Diff(paNew["MyStruct"])
		)
		if diff == nil {
			if c.expected != "" {
				t.Fatal("Expected diff between structs")
			}
		} else if diff.DiffString(0) != c.expected+"\n" {
			t.Errorf("Expected\n%s\n, got\n%s", c.expected+"\n", diff.DiffString(0))
		}
	}
}

func TestInterfaceChange(t *testing.T) {
	const Prefix = "type MyFace interface"
	cases := []struct {
		oldDef   string
		newDef   string
		expected string
	}{
		// basic add/remove
		{
			"{ Stringer; SomeFunc(int) (string) }",
			"{ SomeFunc(int) string; Formatter }",
			`
M	type MyFace interface {
	D	Stringer
	A	Formatter
	}`[1:]},
		// signature changes
		{
			"{ SomeFunc([]byte) uint }",
			"{ SomeFunc([]*byte) uint32 }",
			`
M	type MyFace interface {
	M	-SomeFunc([]byte) uint
		+SomeFunc([]*byte) uint32
	}`[1:]},
		// imported embeds
		{
			"{ somepkg.Server; apkg.Caller }",
			"{ apkg.Caller; otherpkg.Server }",
			`
M	type MyFace interface {
	D	somepkg.Server
	A	otherpkg.Server
	}`[1:]},
		// argument name changes, no diff
		{
			"{ SomeFunc(s string) (c chan bool) }",
			"{ SomeFunc(string) chan bool }",
			"",
		},
	}
	for _, c := range cases {
		var (
			paOld = populateFrom(t, prefixPkg(Prefix+c.oldDef))
			paNew = populateFrom(t, prefixPkg(Prefix+c.newDef))
			diff  = paOld["MyFace"].Diff(paNew["MyFace"])
		)
		if diff == nil {
			if c.expected != "" {
				t.Fatal("Expected diff between interfaces")
			}
		} else if diff.DiffString(0) != c.expected+"\n" {
			t.Errorf("Expected\n%s\n, got\n%s", c.expected+"\n", diff.DiffString(0))
		}
	}
}
