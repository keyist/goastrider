package main

import (
	"bytes"
	"fmt"
	"go/ast"
	"go/printer"
	"go/token"
	"reflect"
	"regexp"
	"strings"
)

// a Node is a wrapper for ast nodes
type Node struct {
	N ast.Node
}

// KeyValue returns the unique string representing a node for use as PackageAPI
// key and the node that the key maps to.  A nil return node indicates that the
// key should not be added.
func (n *Node) KeyValue() (string, *Node) {
	switch t := n.N.(type) {
	case *ast.FuncDecl:
		if t.Recv != nil {
			prefix := nodeToString(t.Recv.List[0].Type)
			if prefix[0] == '*' {
				// given declarations
				//   old: func (r Recv) String()
				//   new: func (r *Recv) String()
				// we want to recognize the change as a modification, not a delete+add
				// so we use the base type for the key
				prefix = prefix[1:]
			}

			return strings.Join([]string{prefix, t.Name.String()}, "."), n
		}
		return t.Name.String(), n
	case *ast.TypeSpec:
		return t.Name.String(), n
	}
	return "", nil
}

func (n *Node) String() string {
	switch t := n.N.(type) {
	case *ast.TypeSpec:
		return strings.Join([]string{"type",
			t.Name.String(), nodeToString(t.Type)}, " ")
	case *ast.FuncDecl:
		return n.typelessSignature("func " + t.Name.String())
	}
	return ""
}

func (n Node) Diff(otherNode *Node) *Diff {
	switch t := n.N.(type) {
	case *ast.FuncDecl:
		return n.FuncDiff("func "+t.Name.String(), otherNode)
	case *ast.TypeSpec:
		nTypeNode := &Node{N: t.Type}
		otherType := otherNode.N.(*ast.TypeSpec)
		otherTypeNode := &Node{N: otherType.Type}
		return nTypeNode.TypeDiff(t.Name.String(), otherTypeNode)
	default:
		return nil
	}
}

func (n Node) SameType(otherNode *Node) bool {
	return reflect.TypeOf(n.N).String() == reflect.TypeOf(otherNode.N).String()
}

func (n Node) ReflectedType() string {
	return reflect.TypeOf(n.N).String()
}

// FuncDiff detects differences in function declarations
func (n *Node) FuncDiff(name string, otherNode *Node) *Diff {
	var (
		thisSig  = n.typelessSignature(name)
		otherSig = otherNode.typelessSignature(name)
	)
	if thisSig != otherSig {
		return &Diff{
			Type: DiffModify,
			Desc: redText("-"+thisSig) + "\n" +
				greenText("+"+otherSig),
		}
	}

	return nil
}

// typelessSignature returns a function declaration with receiver, argument, result names stripped.  It supports FuncDecl and FuncType nodes
func (n *Node) typelessSignature(name string) string {
	var (
		ftype *ast.FuncType
		parts []string
	)
	switch t := n.N.(type) {
	case *ast.FuncDecl:
		if t.Recv != nil {
			parts = append(parts, "("+nodeToString(t.Recv.List[0].Type)+") ")
		}
		ftype = t.Type
	case *ast.FuncType:
		ftype = t
	}
	parts = append(parts, name)
	var (
		args    = strings.Join(fieldListTypes(ftype.Params), ", ")
		results = strings.Join(fieldListTypes(ftype.Results), ", ")
	)
	parts = append(parts, "("+args+")")

	switch numResults := ftype.Results.NumFields(); {
	case numResults == 1:
		parts = append(parts, " "+results)
	case numResults > 1:
		parts = append(parts, " ("+results+")")
	}
	return strings.Join(parts, "")
}

// TypeDiff detects differences in type declarations
func (n *Node) TypeDiff(name string, otherNode *Node) *Diff {
	var (
		this      = n.N
		other     = otherNode.N
		thisType  = nodeToString(this)
		otherType = nodeToString(other)
	)
	diff := &Diff{
		Type: DiffModify,
		Desc: fmt.Sprintf("%s\n%s",
			redText(strings.Join([]string{"-type", name, thisType}, " ")),
			greenText(strings.Join([]string{"+type", name, otherType}, " "))),
	}
	switch {
	case thisType == otherType:
		return nil
	case n.ReflectedType() != otherNode.ReflectedType():
		// note ordering of conditions -- putting this case here allows checking both nodes for equality in subsequent cases
		return diff
	case n.ReflectedType() == "*ast.StructType":
		return n.StructDiff(name, otherNode)
	case n.ReflectedType() == "*ast.InterfaceType":
		return n.InterfaceDiff(name, otherNode)
	default:
		return diff
	}
	panic("Switch not exhaustive")
	return nil
}

func (n *Node) InterfaceDiff(name string, otherNode *Node) *Diff {
	var (
		this     = n.N.(*ast.InterfaceType)
		other    = otherNode.N.(*ast.InterfaceType)
		thisSet  = NewInterfaceSet(this.Methods)
		otherSet = NewInterfaceSet(other.Methods)
	)
	subdiffs := CompareSets(thisSet, otherSet)
	if len(subdiffs) == 0 {
		// TypeDiff matches on string which is an overly slack heuristic for interfaces.  The results from InterfaceSets take precedence.
		return nil
	}

	return &Diff{
		Type:     DiffModify,
		Desc:     strings.Join([]string{"type", name, "interface {\n%s\n}"}, " "),
		Subdiffs: subdiffs,
	}
}

func (n *Node) StructDiff(name string, otherNode *Node) *Diff {
	var (
		this     = n.N.(*ast.StructType)
		other    = otherNode.N.(*ast.StructType)
		thisSet  = NewFieldSet(this.Fields)
		otherSet = NewFieldSet(other.Fields)
	)
	subdiffs := CompareSets(thisSet, otherSet)
	if len(subdiffs) == 0 {
		// TypeDiff matches on string which is an overly slack heuristic for structs.  The results from FieldSets take precedence.
		return nil
	}
	return &Diff{
		Type:     DiffModify,
		Desc:     strings.Join([]string{"type", name, "struct {\n%s\n}"}, " "),
		Subdiffs: subdiffs,
	}
}

// fieldListTypes returns a slice of the string representation of types in a FieldList
func fieldListTypes(fl *ast.FieldList) (types []string) {
	if fl == nil {
		return types
	}
	for _, f := range fl.List {
		if f.Names == nil { // anonymous field
			types = append(types, nodeToString(f.Type))
		} else {
			for i := 0; i < len(f.Names); i++ {
				types = append(types, nodeToString(f.Type))
			}
		}
	}
	return types
}

func nodeToString(n ast.Node) string {
	var (
		buf bytes.Buffer
		// remove comments, specifically '// contains filtered or
		// unexported...' comments from struct/interface declaration output
		re = regexp.MustCompile("//.*?\n")
	)
	printer.Fprint(&buf, &token.FileSet{}, n)
	return re.ReplaceAllString(buf.String(), "")
}
