#!/usr/bin/env bash

workd=$(pwd)
usage="Usage: $0 <revision>\nWhile in a git repository root directory, provide a revision,  The repo will be checked out at <commit> into a temporary directory, and goastrider will compare that revision with the one in pwd\nThis script assumes goastrider is in your PATH"

errout() {
	echo $@
	echo -e $usage
	exit 1
}

cleanup() {
	cd $workd
	if [[ ! -z $tmpd && -d $tmpd ]]; then
		echo "Removing $tmpd"
		rm -r $tmpd
	fi
	exit 1
}
trap cleanup ERR SIGHUP SIGINT SIGTERM

if [[ ! -d $workd/.git ]]; then
	errout "No git repository detected; looking for $workd/.git"
fi

if [[ -z $TMPDIR ]]; then
	errout "Could not determine tmp directory, please set TMPDIR"
fi

if [[ $# -ne 1 ]]; then
	errout "Please provide a revision argument"
fi


if ! git branch -a --contains $1 >/dev/null 2>&1; then
	errout "Could not find revision '$1'"
fi

tmpd=$TMPDIR/goastrider-$(date +%s)

if [[ -d tmpd ]]; then
	errout "Attempted to use $tmpd as destination directory but it already exists"
fi

echo "Cloning into $tmpd"
git clone . $tmpd 2>/dev/null
cd $tmpd
git checkout $1 2>/dev/null
cd $workd

goastrider $tmpd $workd

cleanup
