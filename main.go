package main

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

const (
	Version = "0.6.2"
	Usage   = `Usage: %s <old-path> <new-path>
Outputs a partial semantic diff of Go source files in <old-path> and <new-path>` + "\n"
)

// command-line switches
var (
	FlagWithUnexported bool
	FlagWithTests      bool
	FlagVerbose        bool
	FlagDebug          bool
	FlagShow           string
	FlagCheck          bool
)

var baseOldPath, baseNewPath string

// checkArgs validates the command-line arguments given and outputs errors to stderr
func checkArgs() bool {
	version := flag.Bool("v", false, "print goastrider version")
	flag.Parse()
	if FlagDebug {
		FlagVerbose = true
	}
	if *version {
		fmt.Printf("goastrider version %s\n", Version)
		os.Exit(0)
	}
	if flag.NArg() != 2 {
		flag.Usage()
		return false
	}
	dirError := false
	for i := 0; i < 1; i++ {
		path := flag.Arg(i)
		if fi, e := os.Stat(path); e != nil || !fi.IsDir() {
			fmt.Fprintf(os.Stderr, "%s is not a directory\n", path)
			dirError = true
		}
	}

	return !dirError
}

func init() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage, os.Args[0])
		flag.PrintDefaults()
	}
	const (
		UnexportedUsage = "Set to include unexported (lowercase) declarations"
		TestsUsage      = "Set to include *_test.go files"
		VerboseUsage    = "Set to output warnings to stderr"
		DebugUsage      = "Set to enable debug mode (automatically sets verbose)"
		ShowUsage       = "Turn on/off diffs of a certain type (D=Deletions/A=Additions/M=Modifications/m=Modifications with subdiffs)"
		CheckUsage      = "Returns exit code 2 if any diffs are found"
	)
	flag.BoolVar(&FlagWithUnexported, "with-unexported", false, UnexportedUsage)
	flag.BoolVar(&FlagWithTests, "with-tests", false, TestsUsage)
	flag.BoolVar(&FlagVerbose, "verbose", false, VerboseUsage)
	flag.BoolVar(&FlagDebug, "debug", false, DebugUsage)
	flag.StringVar(&FlagShow, "show", "DAM", ShowUsage)
	flag.BoolVar(&FlagCheck, "check", false, CheckUsage)
}

func main() {
	if !checkArgs() {
		os.Exit(1)
	}
	baseOldPath = flag.Arg(0)
	baseNewPath = flag.Arg(1)
	err := DirDiff()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error parsing: %s.\n", err)
		fmt.Fprintf(os.Stderr, "Please check that\n"+
			"* your user has access to directories %s and %s\n"+
			"* the Go code they contain can build successfully\n",
			baseOldPath, baseNewPath)
		os.Exit(1)
	}
}

func DirDiff() error {
	var (
		longestCommonPathEls []string
		diffs                []*Diff
		errors               []error
	)
	updateLongestCommon := func(path string) {
		// TODO use suffix tree if this gets too slow
		els := strings.Split(path, string(filepath.Separator))
		// we use cap here as a proxy for unused: the longestCommonPath was
		// populated and subsequently emptied as a result of no paths being
		// common, we don't want to add to it again
		if cap(longestCommonPathEls) == 0 {
			longestCommonPathEls = els
			return
		}
		for i, _ := range longestCommonPathEls {
			if i >= len(els) {
				return
			}
			if els[i] != longestCommonPathEls[i] {
				longestCommonPathEls = longestCommonPathEls[:i]
				return
			}
		}
		return
	}
	fn := func(oldPath string, oldFi os.FileInfo, err error) error {
		if err != nil {
			errors = append(errors, fmt.Errorf("%s in %s", err, oldPath))
		}
		if strings.Contains(oldPath, "/.git/") {
			return nil
		}
		newPath := strings.Replace(oldPath, baseOldPath, baseNewPath, 1)
		if newFi, e := os.Stat(newPath); e != nil || !newFi.IsDir() {
			return nil
		}

		// technically unnecessary to replace as longestCommon will catch it, but it speeds up longestCommon
		prefix := strings.Replace(oldPath, baseOldPath, "", 1)

		pathDiffs, err := PackageDiff(oldPath, newPath, prefix)
		if err != nil {
			errors = append(errors, err)
			return nil
		}
		diffs = append(diffs, pathDiffs...)
		for _, d := range pathDiffs {
			updateLongestCommon(d.Desc)
		}
		return nil
	}

	err := filepath.Walk(baseOldPath, fn)
	if err != nil && len(diffs) == 0 {
		return err
	}
	checkFailed := false
	for _, d := range diffs {
		if !d.ShouldShow() {
			continue
		}
		checkFailed = true
		importPath := strings.Replace(d.Desc,
			strings.Join(longestCommonPathEls, string(filepath.Separator)), "", 1)
		d.Desc = d.Colorize("Package " + strings.Trim(importPath, "/"))
		fmt.Println(d.DiffString(0))
	}
	if len(errors) > 0 {
		fmt.Fprintln(os.Stderr, "Errors encountered:")
		for _, e := range errors {
			fmt.Fprintf(os.Stderr, "\t%s\n", e)
		}
		os.Exit(1)
	}
	if FlagCheck && checkFailed {
		os.Exit(2)
	}
	return nil
}

func Warn(s string, a ...interface{}) {
	if !FlagVerbose {
		return
	}
	fmt.Fprintf(os.Stderr, s, a...)
}
